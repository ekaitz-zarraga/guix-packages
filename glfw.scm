(define-module (glfw)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages freedesktop))

(define-public glfw
  (package
    (name "glfw")
    (version "3.3.7")
    (source (origin
              (method git-fetch)
              (uri
                (git-reference
                  (url "https://github.com/glfw/glfw")
                  (commit version)))
              (sha256
                (base32 "1v4fz8w1i71jhmlqf6062glbxvzdwjviqa3wjb9a4qyqg100gxvs"))))
    
    (inputs (list
              libxcursor
              libx11
              libxinerama
              libxrandr
              libxi
              wayland
              wayland-protocols))
    (build-system cmake-build-system)
    (arguments
      `(#:tests? #f))         ; Distributed without tests
    (home-page "https://www.glfw.org")
    (synopsis "A multi-platform library for OpenGL, OpenGL ES, Vulkan, window
and input")
    (description "GLFW is a multi-platform library for OpenGL, OpenGL ES and
Vulkan application development. It provides a simple, platform-independent API
for creating windows, contexts and surfaces, reading input, handling events,
etc.")
    (license license:zlib)))
