(define-module (riscv)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages bootloaders) ; For spike
  )


(define-public spike
  (package
    (name "spike")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/riscv/riscv-isa-sim.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256 (base32 "0cik2m0byfp9ppq0hpg3xyrlp5ag1i4dww7a7872mlm36xxqagg0"))))

    (propagated-inputs
      `(("dtc" ,dtc)))
    (build-system gnu-build-system)
    (home-page "https://github.com/riscv/riscv-isa-sim")
    (description "Spike, the RISC-V ISA Simulator, implements a functional
model of one or more RISC-V harts.")
    (synopsis "RISC-V ISA Simulator")
    (license license:gpl3+)))
